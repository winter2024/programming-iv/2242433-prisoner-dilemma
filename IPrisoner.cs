namespace PrisonerDilemma;

/// <summary>
/// Prisoner that participates in an iterated dilemma
/// </summary>
/// <remarks>
/// Here are the expected steps:
/// <list type="number">
/// <item>Reset is called, to initialize the prisoner</item>
/// <item>GetChoice is called, for the prisoner to decide their action</item>
/// <item>
///   ReceiveOtherChoice is called, the prisoner learns the other's decision
/// </item>
/// <item>Steps 2 and 3 are repeated until the iterated dilemma is over</item>
/// <item>
///   If the prisoner participates in another dilemma, Reset is called again
/// </item>
/// </list>
/// <remarks>
interface IPrisoner
{
  /// <summary>
  /// Display name of the prisoner, used when displaying rankings at the end of
  /// the tournament
  /// </summary>
  public string Name { get; }

  /// <summary>
  /// Gets the next choice (cooperate or defect) the prisoner makes
  /// </summary>
  public PrisonerChoice GetChoice();

  /// <summary>
  /// Prepares the prisoner for a new iterated prisoner dilemma, against a
  /// possibly different prisoner
  /// </summary>
  public void Reset(DilemmaParameters dilemmaParameters);

  /// <summary>
  /// Called during the iterated prisoner dilemma, once both prisoners have made
  /// their choice
  /// </summary>
  public void ReceiveOtherChoice(PrisonerChoice otherChoice);
}
