namespace PrisonerDilemma;

public class PrisonerDilemma : IPrisoner
{
    public string Name { get; } = "Prisoner2242433";

    PrisonerChoice IPrisoner.GetChoice()
    {
        return PrisonerChoice.Defect;
    }

    void IPrisoner.ReceiveOtherChoice(PrisonerChoice otherChoice){ }

    void IPrisoner.Reset(DilemmaParameters dilemmaParameters){ }
}
